package main

import (
	_ "kee/routers"
	"github.com/astaxie/beego"
	"os"
	"fmt"
	"kee/plugin/keenabc"
)


func init() {
	path, _ := os.Getwd()
	fmt.Println(path)
	beego.SetLogger("file", `{
		"filename": "D:\\GOPATH\\src\\kee\\memory.log",
		"maxdays": 365
	}`)
}

func main() {
	//SSS()
	//keenabc.ReadRouter()
	//keenabc.ReadExcel()
	keenabc.AllCache()
	beego.Run()
}