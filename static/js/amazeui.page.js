!function($,UI){
    "use strict";
    function page(data){
        this.$element=data.element;
        this.first=true;
        data.option.curr=parseInt(data.option.curr)||1;
        data.option.theme=data.option.theme||'default';
        data.option.groups=(typeof data.option.groups!="undefined")?parseInt(data.option.groups):5;
        this.option=data.option;
        this._init();
    }

    page.prototype._init=function(){
        if(this.option.groups>0){
            this.option._prev=Math.ceil(this.option.groups/2);
            this.option._next=Math.floor(this.option.groups/2);
            this.option._status={prev:false,next:false};
        }
        if(this.option.before){
            var _this=this;
            this.option.before(this,function(){
                _this._load();
            });
        }else{
            this._load();
        }
    }

    page.prototype._load=function(){
        var option=this.option;
        if(!option.pages||option.pages==1){
            return false;
        }
        var $element = this.$element;
        var $ul=$("<ul style='font-size: 14px;'></ul>");
        $ul.addClass('am-pagination tpl-pagination am-page-'+option.theme);
        $element.html($ul);

        var list=[];
        if(option.curr>1&&option.prev!==false)list.push({key:'prev',value:option.prev||'上一页',page:option.curr-1});
        if(option.first)list.push({key:'first',value:option.first,page:1});
        for(var i=1;i<=option.pages;i++){
            list.push({key:i,value:i,page:i});
        }
        if(option.last)list.push({key:'last',value:option.last,page:option.pages});
        if(option.curr!=option.pages&&option.next!==false)list.push({key:'next',value:option.next||'下一页',page:option.curr+1});
        //groups处理
        var judge=function(option,index){
            var result='<span>...</span>';
            if(index<=((option.curr+option._next)<option.pages?option.curr-option._prev:option.pages-option.groups)){
                result=option._status.prev?"":result;
                option._status.prev=true;
                return result;
            }
            if(index>((option.curr+option._next)<option.groups?option.groups:option.curr+option._next)){
                result=option._status.next?"":result;
                option._status.next=true;
                return result;
            }
            return false;
        }
        //渲染
        var render=function(context,$li,index){
            var option=context.option,r;
            //是否显示分页按钮
            if(option.groups==0&&typeof index=="number"){
                return false;
            }
            //数量
            if(typeof index=="number"&&option.groups<option.pages){
                if((r=judge(option,index))!==false){
                    return r;
                }
            }
            //当前按钮
            if(option.curr==index){
                $li.addClass('am-active');
            }
            return $li;
        }
        var _render=option.render?option.render:render;
        for (var i in list) {
            var $li=$('<li><a href="javascript:" data-page="'+list[i]['page']+'">'+list[i]['value']+'</a></li>');
            var res;
            if(!option.render||!(res=option.render(this,$li,list[i]['key']))){
                res=render(this,$li,list[i]['key']);
            }
            $ul.append(res);
        }
        this._on();
        if(this.option.after){
            var _this=this;
            this.option.after(this,function(){
                _this._jump();
            });
        }else{
            this._jump();
        }
    }

    page.prototype._on=function(){
        var _this=this;
        _this.$element.one('click','li a',function(){
            _this.option.curr=$(this).data('page');
            _this._init();
        });
    }

    page.prototype._jump=function(){
        if(!this.first&&typeof this.option.jump=='string'&&this.option.jump.indexOf('%page%')>-1){
            window.location.href=this.option.jump.replace('%page%',this.option.curr);
            return;
        }
        if(typeof this.option.jump=='function'){
            this.$element.trigger('jump.page.amui');
            this.option.jump(this,this.first);
        }
        this.first=false;
    }

    page.prototype.remove=function(callback){
        this.$element.trigger('remove.page.amui');
        this.$element.remove();
        this.$element.trigger('removed.page.amui');
        if(callback)callback();
    }

    page.prototype.setCurr=function(curr,callback){
        this.option.curr=parseInt(curr);
        this._init();
        if(callback)callback();
    }

    $.fn.extend({
        'page':function(option){
            return new page({
                element:this,
                option:option
            });
        }
    });
    UI.ready(function(context) {
        $('[data-am-page]', context).each(function(){
            var option = UI.utils.parseOptions($(this).attr('data-am-page'));
            //将data api中的参数转为函数
            option.before=window[option.before];
            option.render=window[option.render];
            option.after=window[option.after];
            if(option.jump&&option.jump.indexOf('%page%')==-1){
                option.jump=window[option.jump];
            }
            $(this).page(option);
        })
    });
}(jQuery,jQuery.AMUI);


function showPage(total) {
    var $page = $("#page").page({
        pages: total, //页数
        curr: 1, //当前页
        theme: 'default', //主题
        groups: 5, //连续显示分页数
        prev: '«', //若不显示，设置false即可
        next: '»', //若不显示，设置false即可
        first: false,
        last: false, //false则不显示
        before: function(context, next) { //加载前触发，如果没有执行next()则中断加载
            // console.log('开始加载...');
            // context.time = (new Date()).getTime(); //只是演示，并没有什么卵用，可以保存一些数据到上下文中
            next();
        },
        render: function(context, $element, index) { //渲染[context：对this的引用，$element：当前元素，index：当前索引]
            //逻辑处理
            if (index == 'last') { //虽然上面设置了last的文字为尾页，但是经过render处理，结果变为最后一页
                $element.find('a').html('末页');
                return $element; //如果有返回值则使用返回值渲染
            }
            return false; //没有返回值则按默认处理
        },
        after: function(context, next) { //加载完成后触发
            var time = (new Date()).getTime(); //没有什么卵用的演示
            console.log('分页组件加载完毕，耗时：' + (time - context.time) + 'ms');
            next();
        },
		/*
		 * 触发分页后的回调，如果首次加载时后端已处理好分页数据则需要在after中判断终止或在jump中判断first是否为假
		 */
        jump: function(context, first) {
            console.log('当前第：' + context.option.curr + "页");
            // $("#content").html(thisDate(context.option.curr));
            sendajaxdata(limit, context.option.curr);

        }
    });

    $("#page").on('remove.page.amui', function() {
        console.log('移除前会触发remove.page.amui事件');
    });

    $("#remove").click(function() {
        $page.remove(function() {
            console.log('移除分页组件成功');
        })
    })

    $("#page").on('removed.page.amui', function() {
        console.log('移除后会触发removed.page.amui事件');
    });

    $("#page").on('jump.page.amui', function() {
        console.log('点击分页按钮时会触发jump.page.amui事件');
    });

    $("#set").click(function() {
        var page = $("#curr").val();
        $page.setCurr(page, function() {
            console.log('跳转到第' + page + "页");
        });
    });
}