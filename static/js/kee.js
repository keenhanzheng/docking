/**
 * Created by keen on 2017/7/28.
 */
// 侧边菜单开关

autoLeftNav();
$(window).resize(function() {
    autoLeftNav();
    // console.log($(window).width())
});

function autoLeftNav() {
    $('.tpl-header-switch-button').on('click', function() {
        if ($('.left-sidebar').is('.active')) {
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').removeClass('active');
            }
            $('.left-sidebar').removeClass('active');
        } else {

            $('.left-sidebar').addClass('active');
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').addClass('active');
            }
        }
    })

    if ($(window).width() < 1024) {
        $('.left-sidebar').addClass('active');
    } else {
        $('.left-sidebar').removeClass('active');
    }
}


// 侧边菜单
$('.sidebar-nav-sub-title').on('click', function() {
    $(this).siblings('.sidebar-nav-sub').slideToggle(80)
        .end()
        .find('.sidebar-nav-sub-ico').toggleClass('sidebar-nav-sub-ico-rotate');
})


//激活菜单
var $menulist = $("ul.sidebar-nav li");
$menulist.each(function () {
    var $this = $(this);
    var $linkA = $this.find("a");
    var linkstr = $linkA.attr("href");
    var myUrl = document.URL;
    if (myUrl.indexOf(linkstr) != -1) {
        $linkA.attr("class", "active");
           var $parent = $this.parent();
           if ($parent != undefined && $parent.is("ul")) {
               $parent.attr("style", "display: block;");
               var $a = $parent.siblings("a").children("span");
               $a.attr("class","am-icon-chevron-down am-fr am-margin-right-sm sidebar-nav-sub-ico sidebar-nav-sub-ico-rotate");
               $a.show();
               $this.parent().show();
           }

    }
});