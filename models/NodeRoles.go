package models

import (
	"errors"
	"fmt"
	"github.com/astaxie/beego/orm"
	"reflect"
	"strings"
)

//import "github.com/astaxie/beego/orm"

type NodeRoles struct {
	//base column begin
	Id       int    `orm:"column(id);"`
	NodeId   string `orm:"column(node_id);"`
	RoleId   string `orm:"column(role_id);"`
	ParentId string `orm:"column(parent_id);"`
	Level    string `orm:"column(level);"`
	Priority string `orm:"column(priority);"`

	//base column end

	//foreign begin
	//{{ForeignKey}}
	//foreign end
}

func GetAllNodeRolesCount() (count int64, err error) {
	count, err = Orm.QueryTable("node_roles").Count()
	return
}

func InsertNodeRoles(noderoles NodeRoles) (id int64) {
	o := orm.NewOrm()
	id, err := o.Insert(&noderoles)
	if err == nil {
		fmt.Println(id)
	}
	return id
}

func GetAllNodeRoles(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(NodeRoles))

	for k, v := range query {
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}

	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []NodeRoles
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

func UpdateNodeRoles(id int, noderoles NodeRoles) (err error) {
	o := orm.NewOrm()
	_, err = o.Update(&noderoles)
	return err
}

func DeleteNodeRoles(id int) {
	o := orm.NewOrm()
	if num, err := o.Delete(&NodeRoles{Id: id}); err == nil {
		fmt.Println(num)
	}
}
