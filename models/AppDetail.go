/**
 * Created by keen on 2017/8/12.
 */

package models

type Appdetail struct {
	//base struct begin
	Id         int64     `orm:"column(id);auto"`
	//AppsetId      string    `orm:"column(appset_id);"`
	NodeType   string     `orm:"column(node_type);"`
	NodeId     string    `orm:"column(node_id);"`
	Creator    string `orm:"column(creator);"`
	CreateDate string    `orm:"column(create_date);size(50)"`
	//base struct end

	//
	Appset  *Appset  `orm:"rel(fk)"`
}

func GetAppDetailsByAppId(id string) (appdetails []Appdetail, err error) {
	Orm.QueryTable("appdetail").Filter("Appset", id).RelatedSel().All(&appdetails)
	return
}