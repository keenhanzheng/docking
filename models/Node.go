/**
 * Created by keen on 2017/7/25.
 */

package models

import (
	"reflect"
	"strings"
	"errors"
	"github.com/astaxie/beego/orm"
	"fmt"
	//"kee/plugin/center"
)

type Node struct {
	//base column begin
	Id      int64     `orm:"column(id);auto"`
	Value   string
	Name    string
	Level   string
	Pid     string
	Remark  string
	Status  string
	GroupId string
	Icon    string
	Sort    string
	Creator *DockingworkUser `orm:"rel(fk)"`
	//base column end

	//foreign begin
	//Role *Role `orm:"rel(fk)"`
	//{{ForeignKey}}
	//foreign end
}

func GetAllNodeCount() (count int64, err error) {
	count, err = Orm.QueryTable("node").Count()
	return
}

func InsertNode(node Node) {
	o := orm.NewOrm()
	id, err := o.Insert(&node)
	if err == nil {
		fmt.Println(id)
	}
}



func GetAllNode(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(Node))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []Node
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

type Columns struct {
	ColumnName string
	DataType   string
}

func Abb() (users []Columns, err error) {
	_, err = Orm.Raw("select column_name,data_type from information_schema.COLUMNS where TABLE_SCHEMA = 'dockingwork' and TABLE_NAME = 'awetq' ;").QueryRows(&users)
	return
}
