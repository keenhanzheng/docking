/**
 * Created by keen on 2017/7/25.
 */

package models

type DockingworkUser struct {
	Id          int64     `orm:"column(id);auto"`
	Username    string     `orm:"column(username);"`
	Password    string    `orm:"column(password);"`
	Rules       string `orm:"column(rules);"`
	Status      string `orm:"column(status);"`
	AccessToken string `orm:"column(access_token);"`
	Createtime  string    `orm:"column(createtime);size(50)"`

	//foreign begin
	//{{ForeignKey}}
	//foreign end
}

func GetUserByUser(username string) (user DockingworkUser, err error) {
	err = Orm.QueryTable("dockingwork_user").Filter("username", username).One(&user)
	return
}
