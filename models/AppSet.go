/**
 * Created by keen on 2017/8/12.
 */

package models


type Appset struct {
	Id         int64     `orm:"column(id);auto"`
	AppName    string     `orm:"column(app_name);"`
	AppPath    string    `orm:"column(app_path);"`
	Creator    string `orm:"column(creator);"`
	CreateDate string    `orm:"column(create_date);size(50)"`
	Appdetail        []*Appdetail `orm:"reverse(many)"`
}

func GetAppSetById(id string) (user Appset, err error) {
	err = Orm.QueryTable("appset").Filter("id", id).One(&user)
	return
}