/**
 * Created by keen on 2017/8/2.
 */

package models

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"kee/plugin"
)

var Orm orm.Ormer

func init() {

	plugin.InitCache()

	//orm.RegisterDriver("sqlite", orm.DRSqlite)
	//orm.RegisterDataBase("default", "sqlite3", "db/keedb.sqlite")
	//orm.RegisterModel(new(models.User))
	var connstr string
	connstr = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8",
		beego.AppConfig.String("user"),
		beego.AppConfig.String("password"),
		beego.AppConfig.String("host"),
		beego.AppConfig.String("port"),
		beego.AppConfig.String("db"))
	orm.Debug = true

	//RegisterModel-begin
	orm.RegisterModel(new(DockingworkUser))
	orm.RegisterModel(new(DockingworkPersoncost))
	orm.RegisterModel(new(DockingworkDeviceService))
	orm.RegisterModel(new(Node))
	orm.RegisterModel(new(Appset))
	orm.RegisterModel(new(Appdetail))
	orm.RegisterModel(new(Role))
	orm.RegisterModel(new(NodeRoles))
	//RegisterModel-end

	orm.RegisterDataBase("default", "mysql", connstr, 20, 200)
	Orm = orm.NewOrm()
	Orm.Using("default")
}

