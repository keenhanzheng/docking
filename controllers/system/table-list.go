/**
 * Created by keen on 2017/7/22.
 */

package system

import ."kee/plugin"

type TableListController struct {
	BaseController
}

func (c *TableListController) Get() {
	c.TplName = "system/table-list/table-list.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/table-list/table-list_css.html"
	c.LayoutSections["Scripts"] = "system/table-list/table-list_js.html"
}

