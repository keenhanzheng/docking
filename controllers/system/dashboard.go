/**
 * Created by keen on 2017/7/22.
 */

package system

import ."kee/plugin"

type DashboardController struct {
	BaseController
}

func (c *DashboardController) Get() {
	c.TplName = "system/dashboard/dashboard.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/dashboard/dashboard_css.html"
	c.LayoutSections["Scripts"] = "system/dashboard/dashboard_js.html"
}
