/**
 * Created by keen on 2017/7/22.
 */

package system

import (
	"github.com/astaxie/beego"
	"kee/models"
	"fmt"
	"strconv"
)

type LoginController struct {
	beego.Controller
}

func (c *LoginController) Get() {
	c.TplName = "system/login.html"
}

func (c *LoginController) Post() {
	username := c.GetString("username")
	password := c.GetString("password")
	user, err := models.GetUserByUser(username)
	if password == user.Password && err == nil {
		fmt.Print(user)
		c.SetSession("sess_username", user.Username)
		c.SetSession("sess_rules", user.Rules)
		c.SetSession("sess_userid", strconv.Itoa(int(user.Id)))
		c.Redirect("/dashboard", 302)
		return
	} else {
		c.Redirect("/login", 302)
		return
	}
}
