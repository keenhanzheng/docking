/**
 * Created by keen on 2017/7/22.
 */

package system

import ."kee/plugin"

type SignUpController struct {
	BaseController
}

func (c *SignUpController) Get() {
	c.TplName = "system/sign-up.html"
}

