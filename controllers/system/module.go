package system

import (
	"github.com/astaxie/beego"
	."kee/plugin/keenabc"
)


type ModuleController struct {
	beego.Controller
}

func (m *ModuleController) Get() {
	m.TplName = "app/module/index_modal.html"
}

func (m *ModuleController) Post() {

	moduleName := m.GetString("ModuleName")



	if GenerateView(moduleName) {
		m.Data["json"] = true
	} else {
		m.Data["json"] = false
	}
	m.ServeJSON()
}
