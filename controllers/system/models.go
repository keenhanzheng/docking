package system

import (
	"github.com/astaxie/beego"
	."kee/plugin/keenabc"
	"fmt"
	"strings"
)


type ModelController struct {
	beego.Controller
}

func (m *ModelController) Get() {
	m.TplName = "app/module/model_modal.html"
}

func (m *ModelController) Post() {

	moduleName := m.GetString("ModuleName")
	TableName := m.GetString("TableName")
	ColumnName := m.GetString("ColumnName")
	ColumnType := m.GetString("ColumnType")

	fmt.Println(ColumnName)

	var ColumnNames []string
	var ColumnTypes []string

	ColumnNames = strings.FieldsFunc(ColumnName, split)
	ColumnTypes = strings.FieldsFunc(ColumnType, split)

	if GenerateModel(moduleName, TableName, ColumnNames, ColumnTypes) {
		m.Data["json"] = true
	} else {
		m.Data["json"] = false
	}
	m.ServeJSON()
}

func split(s rune) bool {
	if s == '|' {
		return true
	}
	return false
}