/**
 * Created by keen on 2017/7/22.
 */

package system

import . "kee/plugin"
import (
	"github.com/astaxie/beego"
	"strings"
	"errors"
	"kee/models"
	"strconv"
	"kee/plugin/keenabc"
	"fmt"
	"kee/plugin"
)

type NodeController struct {
	BaseController
}

func (c *NodeController) Get() {
	c.Data["limit"] = beego.AppConfig.String("limit")
	total, _ := models.GetAllNodeCount()
	limit, _ := strconv.Atoi(beego.AppConfig.String("limit"))
	totalpage := int(total) / limit
	if limit*totalpage < int(total) {
		totalpage ++
	}
	c.Data["total"] = strconv.Itoa(totalpage)

	c.TplName = "system/node/index.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/node/css.html"
	c.LayoutSections["Scripts"] = "system/node/js.html"
}

type NodeJsonController struct {
	beego.Controller
}

// @Title get nodes
// @Description get all nodes
// @Success 200 {object} models.Node.Node
// @Failure 404 not found
// @router /nodejson [get]
func (c *NodeJsonController) Get() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllNode(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = l
	}
	c.ServeJSON()

}

type NodeModalController struct {
	beego.Controller
}

func (c *NodeModalController) Get() {
	c.TplName = "system/node/index_modal.html"
}

// @Title get parent_id
// @Description get all the parent_id
// @Success 200 {object} models.ZDTCustomer.Customer
// @Failure 404 not found
// @router /node/getpid [get]
func (c *NodeController) GetPname(){
	c.Data["json"] = keenabc.GetPname()
	c.ServeJSON()
}

func (c *NodeModalController) ToController() {
	c.TplName = "system/node/controller.html"
}

func (c *NodeController) GetIcon() {
	c.Data["json"] = keenabc.GetIcon()
	c.ServeJSON()
}

func (c *NodeController) CreateController() {

	controllerName := c.GetString("Name")
	flags := c.GetStrings("check")

	fmt.Println(controllerName)
	fmt.Println(flags)

	//keenabc.GenerateController(controllerName, flags)
}

func (c *NodeJsonController) GetOne() {
	id,_ := strconv.Atoi(c.GetString("Id"))
	var node models.Node
	err := plugin.GetCache("GetNode.id."+fmt.Sprintf("%d", id), &node)
	if err == nil {
		c.Data["json"] = node
	} else {
		fmt.Println(err)
	}
	c.ServeJSON()
}


func (c *NodeJsonController) Post() {
	b := true

	pName := c.GetString("pName")

	var node models.Node

	node.Pid = strconv.Itoa(keenabc.GetPid(pName))
	node.Name = c.GetString("Name")
	node.Remark = c.GetString("Remark")
	node.Icon = c.GetString("Icon")
	node.Level = c.GetString("Level")
	node.Sort = c.GetString("Sort")
	node.Value = c.GetString("Value")
	node.GroupId = c.GetString("GroupId")



	models.InsertNode(node)

	c.Data["json"] = b
	c.ServeJSON()
}

