/**
 * Created by keen on 2017/7/22.
 */

package system

import ."kee/plugin"

type TableListImgController struct {
	BaseController
}

func (c *TableListImgController) Get() {
	c.TplName = "system/table-list-img/table-list-img.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/table-list-img/table-list-img_css.html"
	c.LayoutSections["Scripts"] = "system/table-list-img/table-list-img_js.html"
}
