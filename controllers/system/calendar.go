/**
 * Created by keen on 2017/7/22.
 */

package system

import ."kee/plugin"

type CalendarController struct {
	BaseController
}

func (c *CalendarController) Get() {
	c.TplName = "system/calendar/calendar.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/calendar/calendar_css.html"
	c.LayoutSections["Scripts"] = "system/calendar/calendar_js.html"
}

