/**
 * Created by keen on 2017/7/22.
 */

package system

import . "kee/plugin"
import (
	"github.com/astaxie/beego"
	"strings"
	"errors"
	"kee/models"
	"strconv"
)

type RoleController struct {
	BaseController
}

func (c *RoleController) Get() {

	c.Data["limit"] = beego.AppConfig.String("limit")
	total, _ := models.GetAllNodeCount()
	limit, _ := strconv.Atoi(beego.AppConfig.String("limit"))
	totalpage := int(total) / limit
	if limit*totalpage < int(total) {
		totalpage ++
	}
	c.Data["total"] = strconv.Itoa(totalpage)

	c.TplName = "system/role/index.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/role/css.html"
	c.LayoutSections["Scripts"] = "system/role/js.html"
}

type RoleJsonController struct {
	beego.Controller
}

func (c *RoleJsonController) Get() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllNode(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = l
	}
	c.ServeJSON()

}

type RoleModalController struct {
	beego.Controller
}

func (c *RoleModalController) Get() {
	c.TplName = "system/role/index_modal.html"
}
