/**
 * Created by keen on 2017/7/22.
 */

package system

import ."kee/plugin"

type ChartController struct {
	BaseController
}

func (c *ChartController) Get() {
	c.TplName = "system/chart/chart.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/chart/chart_css.html"
	c.LayoutSections["Scripts"] = "system/chart/chart_js.html"
}

