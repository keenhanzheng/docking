package system

import (
	"github.com/astaxie/beego"
	."kee/plugin/keenabc"
)

type UpdateController struct {
	beego.Controller
}

type UpdateJsonController struct {
	beego.Controller
}



func (c *UpdateController) Get() {
	rows := GetDB()

	c.Data["json"] = rows
	c.TplName = "app/module/updateDatabase.html"
}

func (c *UpdateJsonController) Get() {

	rows := GetDB()

	c.Data["json"] = rows
	c.ServeJSON()
}

func (c *UpdateJsonController) Post() {
	tableName := c.GetString("TableName")

	NewModel(tableName)
}
