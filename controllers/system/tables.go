/**
 * Created by keen on 2017/7/22.
 */

package system

import ."kee/plugin"

type TablesController struct {
	BaseController
}

func (c *TablesController) Get() {

	c.TplName = "system/tables/tables.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/tables/tables_css.html"
	c.LayoutSections["Scripts"] = "system/tables/tables_js.html"
}

