/**
 * Created by keen on 2017/7/22.
 */

package system

import ."kee/plugin"


type FormController struct {
	BaseController
}

func (c *FormController) Get() {
	c.TplName = "system/form/form.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/form/form_css.html"
	c.LayoutSections["Scripts"] = "system/form/form_js.html"
}

