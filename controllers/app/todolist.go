/**
 * Created by keen on 2017/8/2.
 */

package app

import ."kee/plugin"

type TodolistController struct {
	BaseController
}

func (c *TodolistController) Post() {

}

func (c *TodolistController) Get() {
	c.TplName = "app/todolist/index.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "app/todolist/css.html"
	c.LayoutSections["Scripts"] = "app/todolist/js.html"
}

