package app

import (
	"github.com/astaxie/beego"
	"kee/models"
	"fmt"
	"strings"
	"errors"
	. "kee/plugin"
	"strconv"
	"kee/plugin"
)

type SdvJsonController struct {
	beego.Controller
}

type SdvController struct {
	BaseController
}

func (s *SdvController) Get() {

	s.Data["limit"] = beego.AppConfig.String("limit")
	total, _ := models.GetAllSdvCount()
	limit, _ := strconv.Atoi(beego.AppConfig.String("limit"))
	totalpage := int(total) / limit
	if limit*totalpage < int(total) {
		totalpage ++
	}
	s.Data["total"] = strconv.Itoa(totalpage)

	s.TplName = "app/sdvs/index.html"
	s.LayoutSections = make(map[string]string)
	s.LayoutSections["Css"] = "app/sdvs/css.html"
	s.LayoutSections["Scripts"] = "app/sdvs/js.html"
}

func (s *SdvJsonController) GetOne() {
	fmt.Println(s.Ctx.Request)
	fmt.Println(s.GetString("Id"))
	id,_ := strconv.Atoi(s.GetString("Id"))
	var sdv models.DockingworkDeviceService
	err := plugin.GetCache("GetSdv.id."+fmt.Sprintf("%d", id), &sdv)
	if err == nil {
		fmt.Println("123")
		fmt.Println(sdv)
		s.Data["json"] = sdv
	} else {
		fmt.Println(err)
		s.Data["json"] = models.GetOneSdv(id)
	}
	s.ServeJSON()
}

func (s *SdvJsonController) Get() {

	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	fmt.Println("get")

	// fields: col1,col2,entity.col3
	if v := s.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := s.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := s.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := s.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := s.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := s.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				s.Data["json"] = errors.New("Error: invalid query key/value pair")
				s.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllSdv(query, fields, sortby, order, offset, limit)
	fmt.Println(l)
	if err != nil {
		s.Data["json"] = err.Error()
		beego.Error(err)
	} else {
		s.Data["json"] = l
	}
	s.ServeJSON()
}

type SdvModalController struct {
	beego.Controller
}

func (c *SdvModalController) Get() {
	beego.Debug("Open the sdv modal")
	c.TplName = "app/sdvs/index_modal.html"
}

func (s *SdvModalController) Post(){
	fmt.Println(s.Ctx.Request)
	b := true

	var sdv models.DockingworkDeviceService

	sdv.CreateDate = s.GetString("CreateDate")
	sdv.BackupId = s.GetString("BackupId")
	sdv.Dept = s.GetString("Dept")
	sdv.DeviceId = s.GetString("DeviceId")
	sdv.DeviceTroubleDetails = s.GetString("DeviceTroubleDetails")
	sdv.DeviceTroubleType = s.GetString("DeviceTroubleType")
	sdv.DeviceType = s.GetString("DeviceType")
	sdv.Plugin = s.GetString("Plugin")
	sdv.Police = s.GetString("Police")
	sdv.PoliceId = s.GetString("PoliceId")

	File, _, err := s.GetFile("File")

	fmt.Println(File)

	if err != nil {
		beego.Error(err)
	}

	if s.GetString("ReceiveDeviceDate") != "" {
		sdv.ReceiveDeviceDate = s.GetString("ReceiveDeviceDate")
	}
	if s.GetString("Comment") != "" {
		sdv.Comment = s.GetString("Comment")
	}
	sdv.Unit = s.GetString("Unit")

	err = models.InsertSdv(sdv)
	if err != nil {
		b = false
		beego.Error(err)
	}
	s.Data["json"] = b
	s.ServeJSON()
}

func (s *SdvJsonController) Delete()  {
	b := true

	id, _ := strconv.Atoi(s.GetString("Id"))

	err := models.DeleteSdv(id)
	if err != nil {
		beego.Error(err)
		b = false
	}
	s.Data["json"] = b
	s.ServeJSON()
}

func (s *SdvModalController) Put() {
	b := true

	var sdv models.DockingworkDeviceService

	sdv.Id, _ = strconv.Atoi(s.GetString("Id"))
	sdv.CreateDate = s.GetString("CreateDate")
	sdv.BackupId = s.GetString("BackupId")
	sdv.Dept = s.GetString("Dept")
	sdv.DeviceId = s.GetString("DeviceId")
	sdv.DeviceTroubleDetails = s.GetString("DeviceTroubleDetails")
	sdv.DeviceTroubleType = s.GetString("DeviceTroubleType")
	sdv.DeviceType = s.GetString("DeviceType")
	sdv.Plugin = s.GetString("Plugin")
	sdv.Police = s.GetString("Police")
	sdv.PoliceId = s.GetString("PoliceId")

	if s.GetString("ReceiveDeviceDate") != "" {
		sdv.ReceiveDeviceDate = s.GetString("ReceiveDeviceDate")
	}
	if s.GetString("Comment") != "" {
		sdv.Comment = s.GetString("Comment")
	}
	sdv.Unit = s.GetString("Unit")

	err := models.UpdateSdv(sdv)
	if err != nil {
		b = false
		beego.Error(err)
	}

	s.Data["json"] = b
	s.ServeJSON()
}