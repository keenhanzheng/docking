package app

import (
	"github.com/astaxie/beego"
	"time"
	"kee/models"
)

type CostModalController struct{
	beego.Controller
}

func (f *CostModalController) Get(){
	f.TplName = "app/costs/modal/cost_form.html"
}

func (f *CostModalController) Post(){

	b := true

	cost := f.GetString("newCost")
	note := f.GetString("newNote")
	tNow := time.Now()
	t := tNow.Format("2006-01-02 15:04:05")

	userid := ""
	if f.GetSession("sess_userid") != nil{
		 userid = f.GetSession("sess_userid").(string)
	}

	status := "0"

	var c models.DockingworkPersoncost

	c.Cost = cost
	c.Note = note
	c.Createtime = t

	if userid != "" {
		c.Userid = userid
		c.Status = status
		models.InsertCost(c)
	} else {
		b = false
	}
	f.Data["json"] = b
	f.ServeJSON()
}


