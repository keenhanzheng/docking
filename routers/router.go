package routers

import (
	"kee/controllers/system"
	"github.com/astaxie/beego"
	"kee/controllers/app"
)

func init() {
	//system-begin
	beego.Router("/", &system.LoginController{})
	beego.Router("/login", &system.LoginController{})
	beego.Router("/dashboard", &system.DashboardController{})
	beego.Router("/calendar", &system.CalendarController{})
	beego.Router("/tables", &system.TablesController{})
	beego.Router("/form", &system.FormController{})
	beego.Router("/chart", &system.ChartController{})
	beego.Router("/table-list", &system.TableListController{})
	beego.Router("/table-list-img", &system.TableListImgController{})
	beego.Router("/sign-up", &system.SignUpController{})
	beego.Router("/node", &system.NodeController{})
	beego.Router("/role", &system.RoleController{})
	beego.Router("/appset",&system.AppsetController{})
	beego.AutoRouter(&system.AppsetController{})
	beego.AutoRouter(&system.NodeController{})
	//system-end

	//app-begin
	beego.Router("/costs", &app.CostsController{})
	beego.Router("/sdvs", &app.SdvController{})
	beego.Router("/todolist", &app.TodolistController{})
	beego.Router("/costs/cost_form", &app.CostModalController{})
	//app-end

	//api-begin

	beego.Router("/nodejson", &system.NodeJsonController{})
	beego.AutoRouter(&system.NodeJsonController{})
	beego.Router("/costjson", &app.CostJsonController{})
	beego.Router("/sdvjson", &app.SdvJsonController{})
	beego.AutoRouter(&app.SdvJsonController{})
	beego.Router("/updateJsonDB", &system.UpdateJsonController{})
	//api-end

	//modal-begin
	beego.Router("/nodemodal", &system.NodeModalController{})
	beego.Router("/sdvmodal", &app.SdvModalController{})
	beego.Router("/moduleModal", &system.ModuleController{})
	beego.Router("/updateDB", &system.UpdateController{})
	beego.Router("/modelModal", &system.ModelController{})
	beego.AutoRouter(&system.NodeModalController{})
	//modal-end
}
















































