/**
 * Created by keen on 2017/8/5.
 */

package keenabc

var controllerTpl1 = `package {{packageName}}

import (
	"github.com/astaxie/beego"
)

// {{controllerName}}Controller operations for {{controllerName}}
type {{controllerName}}Controller struct {
	beego.Controller
}


func (c *{{controllerName}}Controller) Post() {

}

func (c *{{controllerName}}Controller) Get() {

}

`

var controllerBaseTpl = `package {{packageName}}

import (
	"github.com/astaxie/beego"
)

// {{controllerName}}Controller operations for {{controllerName}}
type {{controllerName}}Controller struct {
	beego.Controller
}

//begin-controller-base
func (c *{{controllerName}}Controller) Post() {

}

func (c *{{controllerName}}Controller) Get() {

}
//end-controller-base

type {{controllerName}}ModalController struct {

}

type {{controllerName}}JsonController struct {

}

// @Title {{Title}}
// @Description {{Description}}
// {{Param}}
// @Success 200 {{returnType}} models.{{StructName}}.{{StructName}}
// @Failure 400 Invalid email supplied
// @Failure 404 User not found
// @router {{url}} [{{type}}]
func (c *{{controllerName}}JsonController) Get() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAll{{controllerName}}(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = l
	}
	c.ServeJSON()
}



`


//Automate the creation of tables and structures
var modelTpl = `
package models

import (
	"github.com/astaxie/beego/orm"
	"strings"
	"errors"
	"reflect"
	"fmt"
)

//import "github.com/astaxie/beego/orm"

type {{StructName}} struct {
	//base column begin
	{{StructContent}}
	//base column end

	//foreign begin
	//{{ForeignKey}}
	//foreign end
}

func GetAll{{StructName}}Count() (count int64, err error){
	count, err = Orm.QueryTable("{{TableName}}").Count()
	return
}

func Insert{{StructName}}({{structname}} {{StructName}}) (id int64) {
	o := orm.NewOrm()
	id, err := o.Insert(&{{structname}})
	if err == nil {
		fmt.Println(id)
	}
	return id
}

func GetAll{{StructName}}(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error){
	o := orm.NewOrm()
	qs := o.QueryTable(new({{StructName}}))

	for k, v := range query{
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}

	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []{{StructName}}
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

func Update{{StructName}}(id int, {{structname}} {{StructName}}) (err error) {
	o := orm.NewOrm()
   	_, err = o.Update(&{{structname}})
   	return err
}

func Delete{{StructName}}(id int){
	o := orm.NewOrm()
	if num, err := o.Delete(&{{StructName}}{Id: id}); err == nil {
    	fmt.Println(num)
	}
}

`

var controllerJsonTpl = `
//begin-controller-json
// {{controllerName}}Controller operations for {{controllerName}}
type {{controllerName}}Controller struct {
	beego.Controller
}

func (c *{{controllerName}}Controller) Get() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAll{{controllerName}}(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = l
	}
	c.ServeJSON()
}
//end-controller-json
`

var indexTpl = `
<div class="tpl-content-wrapper">
<div class="row-content am-cf">
<div class="row">
<div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
<div class="widget am-cf">
<div class="widget-head am-cf">
<div class="widget-title  am-cf"></div>
                    </div>
                    <div class="widget-body  am-fr">

                        <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                            <div class="am-form-group">
                                <div class="am-btn-toolbar">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <button type="button" class="am-btn am-btn-default am-btn-success js-modal-open"
                                        ><span class="am-icon-plus"></span> 新增
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                            <div class="am-form-group tpl-table-list-select">
                                <select data-am-selected="{btnSize: 'sm'}">
                                    <option value="option1">所有类别</option>
                                    <option value="option2">IT业界</option>
                                    <option value="option3">数码产品</option>
                                    <option value="option3">笔记本电脑</option>
                                    <option value="option3">平板电脑</option>
                                    <option value="option3">只能手机</option>
                                    <option value="option3">超极本</option>
                                </select>
                            </div>
                        </div>
                        <div class="am-u-sm-12 am-u-md-12 am-u-lg-3">
                            <div class="am-input-group am-input-group-sm tpl-form-border-form cl-p">
                                <input type="text" class="am-form-field ">
                                <span class="am-input-group-btn">
            <button class="am-btn  am-btn-default am-btn-success tpl-table-list-field am-icon-search"
                    type="button"></button>
          </span>
                            </div>
                        </div>

                        <div class="am-u-sm-12">
                            <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black "
                                   id="example-r">
                                <thead>
                                <tr>

                                </tr>
                                </thead>

                                <tbody id="{{controllerName}}list"></tbody>
                            </table>
                        </div>
                        <div class="am-u-lg-12 am-cf" id="page"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`

var cssTpl = `
<link rel="stylesheet" href="/static/css/amazeui.datatables.min.css"/>
<link rel="stylesheet" href="/static/css/amazeui.page.css"/>
`

var indexModalTpl = `
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Docking Code</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="/static/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="/static/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" href="/static/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/amazeui.datatables.min.css"/>


    <link href="/static/css/css/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/static/css/css/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/static/css/css/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/static/css/css/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/static/css/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="/static/css/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="/static/css/css/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="/static/css/css/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="/static/css/css/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>
    <script src="/static/js/jquery.min.js"></script>
</head>
<body class="theme-white">
<div class="am-g tpl-g" id="form_wizard_1">
    <div class="portlet-body form">
        <form class="form-horizontal" action="#" id="submit_form" method="POST" novalidate="novalidate">
            <div class="form-wizard">
                <div class="form-body">
                    <ul class="nav nav-pills nav-justified steps">
                        <li class="active">
                            <a href="#tab1" data-toggle="tab" class="step" aria-expanded="true">
                                <span class="number"> 1 </span>
                                <span class="desc">
                                                                    <i class="fa fa-check"></i> 创建控制器 </span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab2" data-toggle="tab" class="step">
                                <span class="number"> 2 </span>
                                <span class="desc">
                                                                    <i class="fa fa-check"></i> 创建模型 </span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab3" data-toggle="tab" class="step active">
                                <span class="number"> 3 </span>
                                <span class="desc">
                                                                    <i class="fa fa-check"></i> 创建视图 </span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab4" data-toggle="tab" class="step">
                                <span class="number"> 4 </span>
                                <span class="desc">
                                                                    <i class="fa fa-check"></i> 完成 </span>
                            </a>
                        </li>
                    </ul>
                    <div id="bar" class="progress progress-striped" role="progressbar">
                        <div class="progress-bar progress-bar-success" style="width: 25%;"></div>
                    </div>
                    <div class="tab-content">
                        <div class="alert alert-success display-none">
                            <button class="close" data-dismiss="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="tab-pane active" id="tab1">
                            <h3 class="block">Provide your account details</h3>

                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="">
                                    <!--<span class="help-block"> Provide your email address </span>-->
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="tab2">
                            <h3 class="block">Provide your profile details</h3>
                            <div class="form-group">
                                <label class="control-label col-md-3">Fullname
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="fullname">
                                    <span class="help-block"> Provide your fullname </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Phone Number
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="phone">
                                    <span class="help-block"> Provide your phone number </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Gender
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <div class="radio-list">
                                        <label>
                                            <input type="radio" name="gender" value="M" data-title="Male"> Male </label>
                                        <label>
                                            <input type="radio" name="gender" value="F" data-title="Female"> Female
                                        </label>
                                    </div>
                                    <div id="form_gender_error"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Address
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="address">
                                    <span class="help-block"> Provide your street address </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">City/Town
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="city">
                                    <span class="help-block"> Provide your city or town </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Country</label>
                                <div class="col-md-4">
                                    <select name="country" id="country_list"
                                            class="form-control select2-hidden-accessible" tabindex="-1"
                                            aria-hidden="true">
                                        <option value=""></option>
                                        <option value="AF">Afghanistan</option>
                                        <option value="AL">Albania</option>
                                        <option value="DZ">Algeria</option>
                                        <option value="AS">American Samoa</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilla</option>
                                        <option value="AR">Argentina</option>
                                        <option value="AM">Armenia</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AU">Australia</option>
                                        <option value="AT">Austria</option>
                                        <option value="AZ">Azerbaijan</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrain</option>
                                        <option value="BD">Bangladesh</option>
                                        <option value="BB">Barbados</option>
                                        <option value="BY">Belarus</option>
                                        <option value="BE">Belgium</option>
                                        <option value="BZ">Belize</option>
                                        <option value="BJ">Benin</option>
                                        <option value="BM">Bermuda</option>
                                        <option value="BT">Bhutan</option>
                                        <option value="BO">Bolivia</option>
                                        <option value="BA">Bosnia and Herzegowina</option>
                                        <option value="BW">Botswana</option>
                                        <option value="BV">Bouvet Island</option>
                                        <option value="BR">Brazil</option>
                                        <option value="IO">British Indian Ocean Territory</option>
                                        <option value="BN">Brunei Darussalam</option>
                                        <option value="BG">Bulgaria</option>
                                        <option value="BF">Burkina Faso</option>
                                        <option value="BI">Burundi</option>
                                        <option value="KH">Cambodia</option>
                                        <option value="CM">Cameroon</option>
                                        <option value="CA">Canada</option>
                                        <option value="CV">Cape Verde</option>
                                        <option value="KY">Cayman Islands</option>
                                        <option value="CF">Central African Republic</option>
                                        <option value="TD">Chad</option>
                                        <option value="CL">Chile</option>
                                        <option value="CN">China</option>
                                        <option value="CX">Christmas Island</option>
                                        <option value="CC">Cocos (Keeling) Islands</option>
                                        <option value="CO">Colombia</option>
                                        <option value="KM">Comoros</option>
                                        <option value="CG">Congo</option>
                                        <option value="CD">Congo, the Democratic Republic of the</option>
                                        <option value="CK">Cook Islands</option>
                                        <option value="CR">Costa Rica</option>
                                        <option value="CI">Cote d'Ivoire</option>
                                        <option value="HR">Croatia (Hrvatska)</option>
                                        <option value="CU">Cuba</option>
                                        <option value="CY">Cyprus</option>
                                        <option value="CZ">Czech Republic</option>
                                        <option value="DK">Denmark</option>
                                        <option value="DJ">Djibouti</option>
                                        <option value="DM">Dominica</option>
                                        <option value="DO">Dominican Republic</option>
                                        <option value="EC">Ecuador</option>
                                        <option value="EG">Egypt</option>
                                        <option value="SV">El Salvador</option>
                                        <option value="GQ">Equatorial Guinea</option>
                                        <option value="ER">Eritrea</option>
                                        <option value="EE">Estonia</option>
                                        <option value="ET">Ethiopia</option>
                                        <option value="FK">Falkland Islands (Malvinas)</option>
                                        <option value="FO">Faroe Islands</option>
                                        <option value="FJ">Fiji</option>
                                        <option value="FI">Finland</option>
                                        <option value="FR">France</option>
                                        <option value="GF">French Guiana</option>
                                        <option value="PF">French Polynesia</option>
                                        <option value="TF">French Southern Territories</option>
                                        <option value="GA">Gabon</option>
                                        <option value="GM">Gambia</option>
                                        <option value="GE">Georgia</option>
                                        <option value="DE">Germany</option>
                                        <option value="GH">Ghana</option>
                                        <option value="GI">Gibraltar</option>
                                        <option value="GR">Greece</option>
                                        <option value="GL">Greenland</option>
                                        <option value="GD">Grenada</option>
                                        <option value="GP">Guadeloupe</option>
                                        <option value="GU">Guam</option>
                                        <option value="GT">Guatemala</option>
                                        <option value="GN">Guinea</option>
                                        <option value="GW">Guinea-Bissau</option>
                                        <option value="GY">Guyana</option>
                                        <option value="HT">Haiti</option>
                                        <option value="HM">Heard and Mc Donald Islands</option>
                                        <option value="VA">Holy See (Vatican City State)</option>
                                        <option value="HN">Honduras</option>
                                        <option value="HK">Hong Kong</option>
                                        <option value="HU">Hungary</option>
                                        <option value="IS">Iceland</option>
                                        <option value="IN">India</option>
                                        <option value="ID">Indonesia</option>
                                        <option value="IR">Iran (Islamic Republic of)</option>
                                        <option value="IQ">Iraq</option>
                                        <option value="IE">Ireland</option>
                                        <option value="IL">Israel</option>
                                        <option value="IT">Italy</option>
                                        <option value="JM">Jamaica</option>
                                        <option value="JP">Japan</option>
                                        <option value="JO">Jordan</option>
                                        <option value="KZ">Kazakhstan</option>
                                        <option value="KE">Kenya</option>
                                        <option value="KI">Kiribati</option>
                                        <option value="KP">Korea, Democratic People's Republic of</option>
                                        <option value="KR">Korea, Republic of</option>
                                        <option value="KW">Kuwait</option>
                                        <option value="KG">Kyrgyzstan</option>
                                        <option value="LA">Lao People's Democratic Republic</option>
                                        <option value="LV">Latvia</option>
                                        <option value="LB">Lebanon</option>
                                        <option value="LS">Lesotho</option>
                                        <option value="LR">Liberia</option>
                                        <option value="LY">Libyan Arab Jamahiriya</option>
                                        <option value="LI">Liechtenstein</option>
                                        <option value="LT">Lithuania</option>
                                        <option value="LU">Luxembourg</option>
                                        <option value="MO">Macau</option>
                                        <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                        <option value="MG">Madagascar</option>
                                        <option value="MW">Malawi</option>
                                        <option value="MY">Malaysia</option>
                                        <option value="MV">Maldives</option>
                                        <option value="ML">Mali</option>
                                        <option value="MT">Malta</option>
                                        <option value="MH">Marshall Islands</option>
                                        <option value="MQ">Martinique</option>
                                        <option value="MR">Mauritania</option>
                                        <option value="MU">Mauritius</option>
                                        <option value="YT">Mayotte</option>
                                        <option value="MX">Mexico</option>
                                        <option value="FM">Micronesia, Federated States of</option>
                                        <option value="MD">Moldova, Republic of</option>
                                        <option value="MC">Monaco</option>
                                        <option value="MN">Mongolia</option>
                                        <option value="MS">Montserrat</option>
                                        <option value="MA">Morocco</option>
                                        <option value="MZ">Mozambique</option>
                                        <option value="MM">Myanmar</option>
                                        <option value="NA">Namibia</option>
                                        <option value="NR">Nauru</option>
                                        <option value="NP">Nepal</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="AN">Netherlands Antilles</option>
                                        <option value="NC">New Caledonia</option>
                                        <option value="NZ">New Zealand</option>
                                        <option value="NI">Nicaragua</option>
                                        <option value="NE">Niger</option>
                                        <option value="NG">Nigeria</option>
                                        <option value="NU">Niue</option>
                                        <option value="NF">Norfolk Island</option>
                                        <option value="MP">Northern Mariana Islands</option>
                                        <option value="NO">Norway</option>
                                        <option value="OM">Oman</option>
                                        <option value="PK">Pakistan</option>
                                        <option value="PW">Palau</option>
                                        <option value="PA">Panama</option>
                                        <option value="PG">Papua New Guinea</option>
                                        <option value="PY">Paraguay</option>
                                        <option value="PE">Peru</option>
                                        <option value="PH">Philippines</option>
                                        <option value="PN">Pitcairn</option>
                                        <option value="PL">Poland</option>
                                        <option value="PT">Portugal</option>
                                        <option value="PR">Puerto Rico</option>
                                        <option value="QA">Qatar</option>
                                        <option value="RE">Reunion</option>
                                        <option value="RO">Romania</option>
                                        <option value="RU">Russian Federation</option>
                                        <option value="RW">Rwanda</option>
                                        <option value="KN">Saint Kitts and Nevis</option>
                                        <option value="LC">Saint LUCIA</option>
                                        <option value="VC">Saint Vincent and the Grenadines</option>
                                        <option value="WS">Samoa</option>
                                        <option value="SM">San Marino</option>
                                        <option value="ST">Sao Tome and Principe</option>
                                        <option value="SA">Saudi Arabia</option>
                                        <option value="SN">Senegal</option>
                                        <option value="SC">Seychelles</option>
                                        <option value="SL">Sierra Leone</option>
                                        <option value="SG">Singapore</option>
                                        <option value="SK">Slovakia (Slovak Republic)</option>
                                        <option value="SI">Slovenia</option>
                                        <option value="SB">Solomon Islands</option>
                                        <option value="SO">Somalia</option>
                                        <option value="ZA">South Africa</option>
                                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                                        <option value="ES">Spain</option>
                                        <option value="LK">Sri Lanka</option>
                                        <option value="SH">St. Helena</option>
                                        <option value="PM">St. Pierre and Miquelon</option>
                                        <option value="SD">Sudan</option>
                                        <option value="SR">Suriname</option>
                                        <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                        <option value="SZ">Swaziland</option>
                                        <option value="SE">Sweden</option>
                                        <option value="CH">Switzerland</option>
                                        <option value="SY">Syrian Arab Republic</option>
                                        <option value="TW">Taiwan, Province of China</option>
                                        <option value="TJ">Tajikistan</option>
                                        <option value="TZ">Tanzania, United Republic of</option>
                                        <option value="TH">Thailand</option>
                                        <option value="TG">Togo</option>
                                        <option value="TK">Tokelau</option>
                                        <option value="TO">Tonga</option>
                                        <option value="TT">Trinidad and Tobago</option>
                                        <option value="TN">Tunisia</option>
                                        <option value="TR">Turkey</option>
                                        <option value="TM">Turkmenistan</option>
                                        <option value="TC">Turks and Caicos Islands</option>
                                        <option value="TV">Tuvalu</option>
                                        <option value="UG">Uganda</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="AE">United Arab Emirates</option>
                                        <option value="GB">United Kingdom</option>
                                        <option value="US">United States</option>
                                        <option value="UM">United States Minor Outlying Islands</option>
                                        <option value="UY">Uruguay</option>
                                        <option value="UZ">Uzbekistan</option>
                                        <option value="VU">Vanuatu</option>
                                        <option value="VE">Venezuela</option>
                                        <option value="VN">Viet Nam</option>
                                        <option value="VG">Virgin Islands (British)</option>
                                        <option value="VI">Virgin Islands (U.S.)</option>
                                        <option value="WF">Wallis and Futuna Islands</option>
                                        <option value="EH">Western Sahara</option>
                                        <option value="YE">Yemen</option>
                                        <option value="ZM">Zambia</option>
                                        <option value="ZW">Zimbabwe</option>
                                    </select><span class="select2 select2-container select2-container--bootstrap"
                                                   dir="ltr" style="width: auto;"><span class="selection"><span
                                        class="select2-selection select2-selection--single" role="combobox"
                                        aria-haspopup="true" aria-expanded="false" tabindex="0"
                                        aria-labelledby="select2-country_list-container"><span
                                        class="select2-selection__rendered" id="select2-country_list-container"><span
                                        class="select2-selection__placeholder">Select</span></span><span
                                        class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span
                                        class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Remarks</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" rows="3" name="remarks"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <h3 class="block">Provide your billing and credit card details</h3>
                            <div class="form-group">
                                <label class="control-label col-md-3">Card Holder Name
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="card_name">
                                    <span class="help-block"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Card Number
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="card_number">
                                    <span class="help-block"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">CVC
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="" class="form-control" name="card_cvc">
                                    <span class="help-block"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Expiration(MM/YYYY)
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="MM/YYYY" maxlength="7" class="form-control"
                                           name="card_expiry_date">
                                    <span class="help-block"> e.g 11/2020 </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Payment Options
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <div class="checkbox-list">
                                        <label>
                                            <input type="checkbox" name="payment[]" value="1"
                                                   data-title="Auto-Pay with this Credit Card."> Auto-Pay with this
                                            Credit Card </label>
                                        <label>
                                            <input type="checkbox" name="payment[]" value="2"
                                                   data-title="Email me monthly billing."> Email me monthly billing
                                        </label>
                                    </div>
                                    <div id="form_payment_error"></div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4">
                            <h3 class="block">Confirm your account</h3>
                            <h4 class="form-section">Account</h4>
                            <div class="form-group">
                                <label class="control-label col-md-3">用户名:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="username"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Email:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="email"></p>
                                </div>
                            </div>
                            <h4 class="form-section">Profile</h4>
                            <div class="form-group">
                                <label class="control-label col-md-3">Fullname:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="fullname"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Gender:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="gender"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Phone:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="phone"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Address:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="address"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">City/Town:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="city"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Country:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="country"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Remarks:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="remarks"></p>
                                </div>
                            </div>
                            <h4 class="form-section">Billing</h4>
                            <div class="form-group">
                                <label class="control-label col-md-3">Card Holder Name:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="card_name"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Card Number:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="card_number"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">CVC:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="card_cvc"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Expiration:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="card_expiry_date"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Payment Options:</label>
                                <div class="col-md-4">
                                    <p class="form-control-static" data-display="payment[]"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="javascript:;" class="btn default button-previous disabled" style="display: none;">
                                <i class="fa fa-angle-left"></i> Back </a>
                            <a href="javascript:;" class="btn btn-outline green button-next" onclick="clicknext()">
                                Continue
                                <i class="fa fa-angle-right"></i>
                            </a>
                            <a href="javascript:;" class="btn green button-submit" style="display: none;"> Submit
                                <i class="fa fa-check"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!--<script src="/static/js/amazeui.min.js"></script>-->
<!--<script src="/static/js/amazeui.datatables.min.js"></script>-->
<!--<script src="/static/js/dataTables.responsive.min.js"></script>-->
<!--[if lt IE 9]>
<script src="/static/css/css/plugins/respond.min.js"></script>
<script src="/static/css/css/plugins/excanvas.min.js"></script>
<script src="/static/css/css/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="/static/css/css/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/static/css/css/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/static/css/css/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/static/css/css/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/static/css/css/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/static/css/css/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/static/js/js/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/static/js/js/form-wizard.js" type="text/javascript"></script>
<script src="/static/css/css/plugins/components-select2.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="/static/js/js/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="/static/js/js/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="/static/js/js/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="/static/js/js/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="/static/js/layer/layer.js"></script>
<script src="/static/css/css/plugins/ui-buttons.min.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>
`

var jsTpl = `
<script>
    var limit = {{.limit}};
    if ({{.total}} =='1')
    {
        sendajaxdata(limit, 0);
    }
    function sendajaxdata(limit, offset) {
        var progress = $.AMUI.progress;
        progress.start();
        offset = limit * (offset - 1 );

        var html = [];
        $.get("/sdvjson?limit=" + limit + "&offset=" + offset, function (data) {
            console.log('getjson');
            for (var i = 0; i < data.length; i++) {
                if(i % 2 === 0){
                    html += '<tr class="gradeX">';
                } else {
                    html += '<tr class="even gradeC">';
                }
                html += '<td>'+ (i + 1) +'</td>';

                html += '<td><div class="indicatorContainer" ></div></td>';
                html += '<td>';
                html += '<div class="tpl-table-black-operation">';
                html += '<a href="javascript:;">';
                html += '<i class="am-icon-pencil"></i> 编辑 </a>';
                html += '<a href="javascript:;" class="tpl-table-black-operation-del">';
                html += '<i class="am-icon-trash"></i> 删除';
                html += '</a>';
                html += '</div>';
                html += '</td>';
                html += '</tr>';
            }
            $('#sdvlist').html(html);
            progress.done();
        });
    }

    showPage({{.total}});

    /*$('.create_node').on('click', function () {
        layer.open({
            type: 2,
            title: '',
//            maxmin: true,
            shadeClose: true, //点击遮罩关闭层
            area:  ['900px', '700px'],
            content: '/nodemodal',
        });
    });*/
    $('#indicatorContainer').radialIndicator({
        barColor: '#87CEEB',
        barWidth: 10,
        initValue: 40,
        roundCorner : true,
        percentage: true
    });

    $('#event').AmazeuiUpload({
        url : 'http://localhost/demo.json'
    });

</script>
`

var Controllers = `package {{packageName}}

import (
	"strings"
	"errors"
	"kee/models"
	"strconv"
	"github.com/astaxie/beego"
)

// {{controllerName}}Controller operations for {{controllerName}}
type {{controllerName}}Controller struct {
	beego.Controller
}

//loading {{controllerName}}
func (c *{{controllerName}}Controller) Get() {
	c.Data["limit"] = beego.AppConfig.String("limit")
	total, _ := models.GetAll{{controllerName}}Count()
	limit, _ := strconv.Atoi(beego.AppConfig.String("limit"))
	totalpage := int(total) / limit
	if limit*totalpage < int(total) {
		totalpage ++
	}
	c.Data["total"] = strconv.Itoa(totalpage)

	c.TplName = "system/{{controllerName}}/index.html"
	c.LayoutSections = make(map[string]string)
	c.LayoutSections["Css"] = "system/{{controllerName}}/css.html"
	c.LayoutSections["Scripts"] = "system/{{controllerName}}/js.html"
}

//{{controllerName}}JsonController for {{controllerName}}Data manipulation
type {{controllerName}}JsonController struct {
	beego.Controller
}

// @Title Get by conditions
// @Description get all {{controllerName}}s
// @Success 200 {object} models.{{controllerName}}
// @Failure 403 :objectId is empty
// @router / [get]
func (c *{{controllerName}}JsonController) Get() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAll{{controllerName}}(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = l
	}
	c.ServeJSON()

}

// @Title Create
// @Description create object
// @Param	body		body 	models.{{controllerName}}	true		"The object content"
// @Success 200 {string} models.{{controllerName}}.Id
// @Failure 403 body is empty
// @router / [post]
func (o *{{controllerName}}JsonController) Post() {
	var ob models.{{controllerName}}
	json.Unmarshal(o.Ctx.Input.RequestBody, &ob)
	id := models.Insert{{controllerName}}(ob)
	o.Data["json"] = map[string]string{"Id": strconv.FormatInt(id,10)}
	o.ServeJSON()
}

// @Title Delete
// @Description delete the {{controllerName}}
// @Param	objectId		path 	string	true		"The Id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 Id is empty
// @router /:Id [delete]
func (o *{{controllerName}}JsonController) Delete() {
	objectId := o.Ctx.Input.Param(":Id")
	Id, _ := strconv.Atoi(objectId)
	models.Delete{{controllerName}}(Id)
	o.Data["json"] = "delete success!"
	o.ServeJSON()
}

// @Title Update
// @Description update the object
// @Param	Id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.{{controllerName}}	true		"The body"
// @Success 200 {object} models.{{controllerName}}
// @Failure 403 :Id is empty
// @router /:Id [put]
func (o *{{controllerName}}JsonController) Put() {
	objectId := o.Ctx.Input.Param(":Id")
	var ob models.{{controllerName}}
	json.Unmarshal(o.Ctx.Input.RequestBody, &ob)

	err := models.Update{{controllerName}}(objectId, ob)
	if err != nil {
		o.Data["json"] = err.Error()
	} else {
		o.Data["json"] = "update success!"
	}
	o.ServeJSON()
}

//{{controllerName}}ModalController for {{controllerName}}Modal
type {{controllerName}}ModalController struct {
	beego.Controller
}

//open {{controllerName}}Modal
func (c *{{controllerName}}ModalController) Get() {
	c.TplName = "system/{{controllerName}}/index_modal.html"
}

func (c *{{controllerName}}ModalController)	Put() {

}

`
