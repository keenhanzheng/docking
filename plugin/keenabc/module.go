package keenabc

import (
	"os"
	"strings"
)

func CreateModule(moduleName string) bool {
	path, _ := os.Getwd()
	//Create app root path
	/*err := os.MkdirAll(path+"/output/b", 0766)
	if err != nil {
		fmt.Println(err)
	}*/

	FileName := moduleName

	/*//Create Controller
	os.MkdirAll(path+"/output/b/controllers", 0766)
	f, err := os.OpenFile(path+"/output/b/controllers/"+FileName+".go", os.O_CREATE|os.O_RDWR, 0766)
	fmt.Println(err)
	controllerTpl = strings.Replace(controllerTpl, "{{packageName}}", "Controller", -1)
	controllerTpl = strings.Replace(controllerTpl, "{{controllerName}}", FileName, -1)
	f.WriteString(controllerTpl)
	f.Close()
*/
	//Create Models
	os.MkdirAll(path+"/models", 0766)


	//Create View
	os.MkdirAll(path+"/views/" + FileName, 0766)
	index, _ := os.OpenFile(path+"/views/"+ FileName +"/index.html", os.O_CREATE|os.O_RDWR, 0766)
	css, _ := os.OpenFile(path+"/views/"+ FileName +"/css.html", os.O_CREATE|os.O_RDWR, 0766)
	os.OpenFile(path+"/views/"+ FileName +"/js.html", os.O_CREATE|os.O_RDWR, 0766)
	indexModal, _ := os.OpenFile(path+"/views/"+ FileName +"/index_modal.html", os.O_CREATE|os.O_RDWR, 0766)

	indexTpl = strings.Replace(indexTpl, "{{controllerName}}", FileName, -1)
	index.WriteString(indexTpl)

	css.WriteString(cssTpl)

	indexModal.WriteString(indexModalTpl)


	//Create Static
	os.MkdirAll(path+"/static", 0766)
	//Create Routers
	os.MkdirAll(path+"/routes", 0766)

	return true
}