package keenabc

import (
	"os"
	"strings"
)

func GenerateView(moduleName string) bool {
	path, _ := os.Getwd()
	//fmt.Println(path)

	FileName := moduleName

	//Create View
	os.MkdirAll(path+"/views/app/" + FileName, 0766)
	index, _ := os.OpenFile(path+"/views/app/"+ FileName +"/index.html", os.O_CREATE|os.O_RDWR, 0766)
	css, _ := os.OpenFile(path+"/views/app/"+ FileName +"/css.html", os.O_CREATE|os.O_RDWR, 0766)
	js, _ := os.OpenFile(path+"/views/app/"+ FileName +"/js.html", os.O_CREATE|os.O_RDWR, 0766)
	indexModal, _ := os.OpenFile(path+"/views/app/"+ FileName +"/index_modal.html", os.O_CREATE|os.O_RDWR, 0766)

	indexTpl = strings.Replace(indexTpl, "{{controllerName}}", FileName, -1)
	index.WriteString(indexTpl)

	js.WriteString(jsTpl)

	css.WriteString(cssTpl)

	indexModal.WriteString(indexModalTpl)

	return true
}