package keenabc

import (
	"strings"
	"fmt"
	"database/sql"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/xuri/excelize"
	"os"
	"kee/models"
)



func SqlConnect() orm.Ormer {
	var connstr string
	connstr = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8",
		beego.AppConfig.String("user"),
		beego.AppConfig.String("password"),
		beego.AppConfig.String("host"),
		beego.AppConfig.String("port"),
		beego.AppConfig.String("db"))
	sql.Open("mysql", connstr)

	o := orm.NewOrm()
 	return o
}

/*func Log() {
	beego.SetLogger("file", `{"filename":"project.log","level":7,"maxlines":0,"maxsize":0,"daily":true,"maxdays":10}`)
}*/

func NameChange(oldName string) string {

	kv := strings.Split(oldName, "_")
	var upperStr string

	for i := 0; i < len(kv); i++ {
		vv := []rune(kv[i])
		for j := 0; j < len(vv); j++ {
			if j == 0 {
				vv[j] -= 32
				upperStr += string(vv[j]) // + string(vv[i+1])
			} else {
				upperStr += string(vv[j])
			}
		}
	}

	return upperStr
}

type IdAndLevel struct{
	Level string
	Id int
}

func GetPname() []string {
	o := SqlConnect()

	var nodes []IdAndLevel
	var pid []int
	o.Raw("SELECT level, id FROM node").QueryRows(&nodes)

	for i := 0; i< len(nodes); i++ {
		if nodes[i].Level == "0" {
			pid = append(pid, nodes[i].Id)
		}
	}

	var pName []string
	var tempName string

	for i := 0; i < len(pid); i++ {
		o.Raw("SELECT name FROM node WHERE id = ?", pid[i]).QueryRow(&tempName)
		pName = append(pName, tempName)
	}

	return pName
}

func GetPid(pName string) int {
	o := SqlConnect()

	var Pid int
	o.Raw("SELECT id FROM node WHERE name = ?", pName).QueryRow(&Pid)

	return Pid
}

type Icon struct {
	Name string
	Value string
}

func GetIcon() []Icon {
	o := SqlConnect()

	var icons []Icon
	o.Raw("SELECT name, value FROM icon").QueryRows(&icons)

	return icons


	/*query, query_err := goquery.NewDocument("http://amazeui.org/css/icon")
	if query_err != nil {
		fmt.Println(query_err)
	}

	icon_arr := make(map[int]Icon)

	query.Find("div .doc-example").Find("section").Find("li").Each(func(i int, s *goquery.Selection) {
		i_class,_ := s.Find("i").Attr("class")

		r := Icon{}

		r.Value = i_class

		a := s.Find("a").Text()
		r.Name = a
		fmt.Println(r.Value)

		icon_arr[i] = r
	})

	var connstr string
	connstr = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8",
		beego.AppConfig.String("user"),
		beego.AppConfig.String("password"),
		beego.AppConfig.String("host"),
		beego.AppConfig.String("port"),
		beego.AppConfig.String("db"))

	db, err := sql.Open("mysql", connstr)
	if err != nil {
		fmt.Println(err)
	}

	var strsql string

	strsql = "INSERT INTO icon (name, value) VALUES('"
	for i := 0 ; i < len(icon_arr); i++ {
		if i != len(icon_arr) - 1 {
			strsql += icon_arr[i].Name + "','" + icon_arr[i].Value + "'),('"
		} else {
			strsql += icon_arr[i].Name + "','" + icon_arr[i].Value + "');"
		}
	}

	db.Query(strsql)*/
}

func ReadExcel() {

	path, _ := os.Getwd()

	xlsx, err := excelize.OpenFile(path + "/Sdv.xlsx")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// Get value from cell by given sheet index and axis.

	var sdv models.DockingworkDeviceService
	rows := xlsx.GetRows("Sheet2")
	for i, row := range rows {
		if i >= 2 {
			time :=strings.Split(row[0], "-")
			sdv.CreateDate = "20" + time[2] + "-" + time[0] + "-" + time[1]
			sdv.DeviceId = row[1]
			sdv.DeviceType = row[2]
			sdv.DeviceTroubleType = row[3]
			sdv.DeviceTroubleDetails = row[4]
			sdv.Police = row[5]
			sdv.PoliceId = row[6]
			sdv.Dept = row[7]
			sdv.Unit = row[8]
			sdv.BackupId = row[9]
			sdv.Plugin = row[10]
			if row[12] != "" {
				ReceiveDate := strings.Split(row[12], "-")
				if ReceiveDate != nil {
					sdv.ReceiveDeviceDate = "20" + ReceiveDate[2] + "-" + time[0] + "-" + time[1]
				}
			}
			sdv.Comment = row[13]
		}
		models.InsertSdv(sdv)
	}
}