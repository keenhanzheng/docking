package keenabc

import (
	"kee/models"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
	"fmt"
	"kee/plugin"
)

func AllCache() {
	Sdv()
	Cost()
	Node()
	Role()
	NodeRoles()
}

func Sdv() {
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.DockingworkDeviceService))

	var l []models.DockingworkDeviceService

	if _, err := qs.All(&l); err == nil {
		for _, v := range l {
			cache_expire, _ := beego.AppConfig.Int("cache_expire")
			plugin.SetCache("GetSdv.id."+fmt.Sprintf("%d", v.Id), v, cache_expire)
		}
	}
}

func Cost() {
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.DockingworkPersoncost))

	var l []models.DockingworkPersoncost

	if _, err := qs.All(&l); err == nil {
		for _, v := range l {
			cache_expire, _ := beego.AppConfig.Int("cache_expire")
			plugin.SetCache("GetCost.id."+fmt.Sprintf("%d", v.Id), v, cache_expire)
		}
	}
}

func User() {
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.DockingworkUser))

	var l []models.DockingworkUser

	if _, err := qs.All(&l); err == nil {
		for _, v := range l {
			cache_expire, _ := beego.AppConfig.Int("cache_expire")
			plugin.SetCache("GetUser.id."+fmt.Sprintf("%d", v.Id), v, cache_expire)
		}
	}
}

func Node() {
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.Node))

	var l []models.Node

	if _, err := qs.All(&l); err == nil {
		for _, v := range l {
			cache_expire, _ := beego.AppConfig.Int("cache_expire")
			plugin.SetCache("GetNode.id."+fmt.Sprintf("%d", v.Id), v, cache_expire)
		}
	}
}

func Role() {
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.Role))

	var l []models.Role

	if _, err := qs.All(&l); err == nil {
		for _, v := range l {
			cache_expire, _ := beego.AppConfig.Int("cache_expire")
			plugin.SetCache("GetRole.id."+fmt.Sprintf("%d", v.Id), v, cache_expire)
		}
	}
}

func NodeRoles() {
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.NodeRoles))

	var l []models.NodeRoles

	if _, err := qs.All(&l); err == nil {
		for _, v := range l {
			cache_expire, _ := beego.AppConfig.Int("cache_expire")
			plugin.SetCache("GetNodeRoles.id."+fmt.Sprintf("%d", v.Id), v, cache_expire)
		}
	}
}
