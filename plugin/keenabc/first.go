/**
 * Created by keen on 2017/7/30.
 */

package keenabc

//import (
//	//"os"
//	//"fmt"
//	//"strings"
//	//"kee/plugin/keenabc"
//)

var controllerTpl = `package {{packageName}}

import (
	"github.com/astaxie/beego"
)

// {{controllerName}}JsonController operations for {{controllerName}}
type {{controllerName}}JsonController struct {
	beego.Controller
}

type {{controllerName}}Controller struct {
	BaseController
}

func (c *{{controllerName}}Controller) Get() {
	s.TplName = "app/{{controllerName}}/index.html"
	s.LayoutSections = make(map[string]string)
	s.LayoutSections["Css"] = "app/{{controllerName}}/css.html"
	s.LayoutSections["Scripts"] = "app/{{controllerName}}/js.html"
}

// URLMapping ...
func (c *{{controllerName}}JsonController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Create
// @Description create {{controllerName}}
// @Param	body		body 	models.{{controllerName}}	true		"body for {{controllerName}} content"
// @Success 201 {object} models.{{controllerName}}
// @Failure 403 body is empty
// @router / [post]
func (c *{{controllerName}}JsonController) Post() {

}

// GetOne ...
// @Title GetOne
// @Description get {{controllerName}} by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.{{controllerName}}
// @Failure 403 :id is empty
// @router /:id [get]
func (c *{{controllerName}}JsonController) GetOne() {

}

// GetAll ...
// @Title GetAll
// @Description get {{controllerName}}
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.{{controllerName}}
// @Failure 403
// @router / [get]
func (c *{{controllerName}}JsonController) GetAll() {

}

// Put ...
// @Title Put
// @Description update the {{controllerName}}
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.{{controllerName}}	true		"body for {{controllerName}} content"
// @Success 200 {object} models.{{controllerName}}
// @Failure 403 :id is not int
// @router /:id [put]
func (c *{{controllerName}}JsonController) Put() {

}

// Delete ...
// @Title Delete
// @Description delete the {{controllerName}}
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *{{controllerName}}JsonController) Delete() {

}
`