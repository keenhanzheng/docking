/**
 * Created by keen on 2017/7/23.
 */

package plugin

import (
	"github.com/astaxie/beego"
	//"strings"
	//."kee/plugin"
	//"kee/plugin/center"
	"fmt"
)

type NestPreparer interface {
	NestPrepare()
}

type BaseController struct {
	beego.Controller
}

func (r *BaseController) Prepare() {
	//group := "controllers"
	controller, action := r.GetControllerAndAction()
	fmt.Println(controller)
	fmt.Println(action)
	r.Layout = "system/layout.html"
	//浏览器兼容性
	//r.Data["Respond"] = r.GetSession("respond")
	//模块数据库查询
	//var uid interface{}
	//r.GetSession("abcc")
	//uid = r.GetSession("userid")
	//id, err := center.CheckAction(controller, action, group)
	//if err == nil {
	//	a, err := center.GetAction(id)
	//	if err == nil && uid == nil && !a.Ispublic {
	//		p, err := center.GetAction(a.Pid)
	//		if err == nil {
	//			r.Redirect(beego.URLFor(p.Group+"."+p.Controller+"."+p.Action), 302)
	//		}
	//	}
		//用户权限验证



		//页面模板加载公用部分
		//r.Layout = "public/Layout.html"
		//controller = strings.ToLower(strings.Replace(controller, "Controller", "", 1))
		//path := group + "/" + controller + "/{Section}/" + strings.ToLower(action) + ".html"
		//r.TplName = strings.Replace(path, "{Section}", "Content", 1)
		//r.LayoutSections = make(map[string]string)
		//for _, str := range []string{"SideBar", "HtmlHead", "Scripts"} {
		//	r.LayoutSections[str] = strings.Replace(path, "{Section}", str, 1)
		//}
		////页面自动生成
		////if a.Actiontype == 2 {
		//	CreateHtmlFile(r.Layout, r.TplName, r.LayoutSections)
		////}
		////公共数据初始化
		//if r.Ctx.Request.Method == "GET" {
		//	beego.ReadFromRequest(&r.Controller)
		//}
		//后续控制器初始化
		//if app, ok := r.AppController.(NestPreparer); ok && a.Actiontype == 2 {
		//	app.NestPrepare()
		//}
	//} else {
	//	r.Redirect(beego.URLFor("RootController.Error"), 302)
	//}
}
