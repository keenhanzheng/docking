package center

import (
	"github.com/astaxie/beego/orm"
	"errors"
)

type DockingworkPersoncost struct {
	Id int64
	Cost int64
	Staus string
	Note string
	Createtime string
	Userid int64
}

func (c *DockingworkPersoncost) TableName() string {
	return "dockingwork_personcost"
}

func init() {
	orm.RegisterModel(new(DockingworkPersoncost))
}

//get cost list
func GetCostlist(page int64, page_size int64, sort string) (costs []orm.Params, count int64) {
	o := orm.NewOrm()
	cost := new(DockingworkPersoncost)
	qs := o.QueryTable(cost)
	var offset int64
	if page <= 1 {
		offset = 0
	} else {
		offset = (page - 1) * page_size
	}
	qs.Limit(page_size, offset).OrderBy(sort).Values(&costs, "Id", "Cost", "Staus", "Note", "Createtime", "Userid")
	count, _ = qs.Count()
	return costs, count
}

func ReadCost(nid int64) (DockingworkPersoncost, error) {
	o := orm.NewOrm()
	cost := DockingworkPersoncost{Id: nid}
	err := o.Read(&cost)
	if err != nil {
		return cost, err
	}
	return cost, nil
}

//insert cost
func AddCost(n *DockingworkPersoncost) (int64, error) {
	o := orm.NewOrm()
	cost := new(DockingworkPersoncost)
	cost.Note = n.Note
	cost.Staus = n.Staus
	cost.Createtime = n.Createtime
	cost.Cost = n.Cost
	cost.Userid = n.Userid
	id, err := o.Insert(cost)
	return id, err
}

//更新用户
func UpdateCost(n *DockingworkPersoncost) (int64, error) {
	o := orm.NewOrm()
	cost := make(orm.Params)
	if len(n.Createtime) > 0 {
		cost["Createtime"] = n.Createtime
	}
	if len(n.Staus) > 0 {
		cost["Staus"] = n.Staus
	}
	if n.Cost != 0 {
		cost["Cost"] = n.Cost
	}
	if len(n.Note) > 0 {
		cost["Note"] = n.Note
	}
	if n.Userid != 0 {
		cost["Userid"] = n.Userid
	}
	if len(cost) == 0 {
		return 0, errors.New("update field is empty")
	}
	var table DockingworkPersoncost
	num, err := o.QueryTable(table).Filter("Id", n.Id).Update(cost)
	return num, err
}

func DelCostById(Id int64) (int64, error) {
	o := orm.NewOrm()
	status, err := o.Delete(&DockingworkPersoncost{Id: Id})
	return status, err
}