package center

import (
	"github.com/astaxie/beego/orm"
	"errors"
)

type DockingworkDeviceService struct {
	BackupId	string
	Comment	string
	CreateDate	string
	Dept	string
	DeviceId	string
	DeviceTroubleDetails	string
	DeviceTroubleType	string
	DeviceType	string
	Id	int
	Plugin	string
	Police	string
	PoliceId	string
	ReceiveDeviceDate	string
	Unit	string
}

func (s *DockingworkDeviceService) TableName() string {
	return "dockingwork_device_service"
}

func init() {
	orm.RegisterModel(new(DockingworkDeviceService))
}


//get sdv list
func GetSdvlist(page int64, page_size int64, sort string) (costs []orm.Params, count int64) {
	o := orm.NewOrm()
	sdv := new(DockingworkDeviceService)
	qs := o.QueryTable(sdv)
	var offset int64
	if page <= 1 {
		offset = 0
	} else {
		offset = (page - 1) * page_size
	}
	qs.Limit(page_size, offset).OrderBy(sort).Values(&costs, "Id", "Cost", "Staus", "Note", "Createtime", "Userid")
	count, _ = qs.Count()
	return costs, count
}

func ReadSdv(nid int) (DockingworkDeviceService, error) {
	o := orm.NewOrm()
	sdv := DockingworkDeviceService{Id: nid}
	err := o.Read(&sdv)
	if err != nil {
		return sdv, err
	}
	return sdv, nil
}

//insert sdv
func AddSdv(n *DockingworkDeviceService) (int64, error) {
	o := orm.NewOrm()
	sdv := new(DockingworkDeviceService)

	sdv.BackupId = n.BackupId
	sdv.Comment = n.Comment
	sdv.CreateDate = n.CreateDate
	sdv.Dept = n.Dept
	sdv.DeviceId = n.DeviceId
	sdv.DeviceTroubleDetails = n.DeviceTroubleDetails
	sdv.DeviceTroubleType = n.DeviceTroubleType
	sdv.DeviceType = n.DeviceType
	sdv.Id = n.Id
	sdv.Plugin = n.Plugin
	sdv.Police = n.Police
	sdv.PoliceId = n.PoliceId
	sdv.ReceiveDeviceDate = n.ReceiveDeviceDate
	sdv.Unit = n.Unit

	id, err := o.Insert(sdv)
	return id, err
}

//更新用户
func UpdateSdv(n *DockingworkDeviceService) (int64, error) {
	o := orm.NewOrm()
	sdv := make(orm.Params)
	if len(n.Unit) > 0 {
		sdv["Unit"] = n.Unit
	}
	if len(n.ReceiveDeviceDate) > 0 {
		sdv["ReceiveDeviceDate"] = n.ReceiveDeviceDate
	}
	if len(n.PoliceId) > 0 {
		sdv["PoliceId"] = n.PoliceId
	}
	if len(n.Police) > 0 {
		sdv["Police"] = n.Police
	}
	if len(n.Plugin) > 0 {
		sdv["Plugin"] = n.Plugin
	}
	if len(n.DeviceType) > 0 {
		sdv["DeviceType"] = n.DeviceType
	}
	if len(n.DeviceTroubleType) > 0 {
		sdv["DeviceTroubleType"] = n.DeviceTroubleType
	}
	if len(n.DeviceTroubleDetails) > 0 {
		sdv["DeviceTroubleDetails"] = n.DeviceTroubleDetails
	}
	if len(n.DeviceId) > 0 {
		sdv["DeviceId"] = n.DeviceId
	}
	if len(n.CreateDate) > 0 {
		sdv["CreateDate"] = n.CreateDate
	}
	if len(n.Dept) > 0 {
		sdv["Dept"] = n.Dept
	}
	if len(n.Comment) > 0 {
		sdv["Comment"] = n.Comment
	}
	if len(n.BackupId) > 0 {
		sdv["BackupId"] = n.BackupId
	}
	if len(sdv) == 0 {
		return 0, errors.New("update field is empty")
	}
	var table DockingworkDeviceService
	num, err := o.QueryTable(table).Filter("Id", n.Id).Update(sdv)
	return num, err
}

func DelSdvById(Id int) (int64, error) {
	o := orm.NewOrm()
	status, err := o.Delete(&DockingworkDeviceService{Id: Id})
	return status, err
}